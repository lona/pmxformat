buildscript {
    repositories {
        maven {
            url = uri("https://maven.aliyun.com/repository/central")
            name = "aliyun-central"
        }
    }
}

plugins {
    val kotlinVersion = "1.5.10"
    kotlin("jvm") version kotlinVersion
//    id("maven")
    id("maven-publish")
    id("org.jetbrains.dokka") version "1.4.32"
}

project.group = "com.lonacloud"
project.version = "0.1.0"

repositories {
    maven {
        url = uri("https://maven.aliyun.com/repository/central")
        name = "aliyun-central"
    }
}

dependencies {
    //gson
    implementation("com.google.code.gson:gson:2.8.7")
    //guava
    implementation("com.google.guava:guava:21.0")
    //joml 数学库
    implementation("org.joml:joml:1.10.1")
    //lombok
    compileOnly("org.projectlombok:lombok:1.18.8")
    annotationProcessor("org.projectlombok:lombok:1.18.8")
    testImplementation("org.projectlombok:lombok:1.18.12")
    testAnnotationProcessor("org.projectlombok:lombok:1.18.12")
}

publishing {
    repositories {
        maven {
            val releaseRepoUrl = uri("https:///repository/maven-releases/")
            val snapshotRepoUrl = uri("https:///repository/maven-snapshots/")
            url = if (project.version.toString().endsWith("-SNAPSHOT")) snapshotRepoUrl else releaseRepoUrl
            credentials {
                username = "114514"
                password = "1145141919810"
            }
        }
    }
    publications {
        create<MavenPublication>("maven") {
            groupId = "${project.group}"
            artifactId = "${rootProject.name.toLowerCase()}"
            version = "${project.version}"
            pom {
                this.description.set("PMX Format Parser")
                this.url.set("https://gitee.com/lona/pmxformat")
                this.licenses {
                    name.set("GNU General Public License v3.0")
                    url.set("https://www.gnu.org/licenses/gpl-3.0.en.html")
                }
                developers {
                    developer{
                        id.set("lona-cn")
                        name.set("lona")
                        email.set("1307693959@qq.com")
                    }
                }
            }
            from(components["kotlin"])
        }
    }
}