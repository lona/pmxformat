//package com.lonacloud.rosefinch.modelloader.pmx;
//
//
//import com.lonacloud.sirius.client.api.renderer.Camera;
//import com.lonacloud.sirius.client.api.renderer.texture.Texture;
//import com.lonacloud.sirius.client.impl.renderer.model.part.BakedElement;
//import com.lonacloud.sirius.client.impl.renderer.model.part.BakedPart;
//import com.lonacloud.rosefinch.modelloader.pmx.data.PMXMaterialData;
//import com.lonacloud.rosefinch.modelloader.pmx.data.PMXTextureData;
//import com.lonacloud.rosefinch.modelloader.pmx.data.PMXVertexData;
//import com.lonacloud.rosefinch.modelloader.pmx.deform.BDEF;
//import com.lonacloud.rosefinch.modelloader.pmx.deform.QDEF;
//import com.lonacloud.rosefinch.modelloader.pmx.deform.SDEF;
//import com.lonacloud.sirius.client.impl.renderer.shader.*;
//import com.lonacloud.sirius.client.impl.renderer.shader.uniform.ShaderUniformFloat;
//import com.lonacloud.sirius.client.impl.renderer.shader.uniform.ShaderUniformInt;
//import com.lonacloud.sirius.client.impl.renderer.shader.uniform.ShaderUniformMat;
//import com.lonacloud.sirius.client.impl.renderer.texture.TextureManagerImpl;
//import com.lonacloud.sirius.common.util.SIOUtil;
//import lombok.val;
//import org.joml.Matrix4f;
//import org.lwjgl.BufferUtils;
//
//import javax.imageio.ImageIO;
//import java.awt.*;
//import java.awt.image.BufferedImage;
//import java.io.File;
//import java.io.IOException;
//import java.nio.ByteBuffer;
//import java.nio.IntBuffer;
//import java.util.HashMap;
//import java.util.LinkedList;
//import java.util.List;
//import java.util.Map;
//
//import static org.lwjgl.opengl.GL11.*;
//import static org.lwjgl.opengl.GL13.GL_MULTISAMPLE;
//import static org.lwjgl.opengl.GL15.*;
//
///**
// * @author lona, 1307693959@qq.com
// * @version 1.0
// **/
//public class PMXModel {
//    private static final Shader shader;
//    private final Camera camera;
//    private final Map<BakedPart, ShaderParams> partRenderers = new HashMap<>();
//
//    static {
//        //着色器
//        byte[] vertexShaderBytes;
//        try {
//            vertexShaderBytes = SIOUtil.readAll(PMXModel.class.getClassLoader().getResourceAsStream("shader/pmx.vsh"));
//        } catch (IOException e) {
//            throw new RuntimeException("读取着色器错误");
//        }
//        val vertexShaderStr = new String(vertexShaderBytes);
//        byte[] fragmentShaderBytes;
//        try {
//            fragmentShaderBytes = SIOUtil.readAll(PMXModel.class.getClassLoader().getResourceAsStream("shader/pmx.fsh"));
//        } catch (IOException e) {
//            throw new RuntimeException("读取着色器错误");
//        }
//        val fragmentShaderStr = new String(fragmentShaderBytes);
//        List<VertexAttributePointer> vertexAttributePointers = new LinkedList<>();
//        final int stride = (3 + 3 + 2 + 4) * Float.BYTES + (1 + 4) * Integer.BYTES;
////        final int stride = 8 * 4;
//        vertexAttributePointers.add(new VertexAttributePointer("position", 3, GL_FLOAT, false, stride, 0));
//        vertexAttributePointers.add(new VertexAttributePointer("normal", 3, GL_FLOAT, false, stride, 3 * Float.BYTES));
//        vertexAttributePointers.add(new VertexAttributePointer("texCoords", 2, GL_FLOAT, false, stride, (3 + 3) * Float.BYTES));
//        vertexAttributePointers.add(new VertexAttributePointer("weightDeformType", 1, GL_INT, false, stride, (3 + 3 + 2) * Float.BYTES));
//        vertexAttributePointers.add(new VertexAttributePointer("boneIndices", 4, GL_INT, false, stride, (3 + 3 + 2) * Float.BYTES + Integer.BYTES));
//        vertexAttributePointers.add(new VertexAttributePointer("boneWeights", 4, GL_FLOAT, false, stride, (3 + 3 + 2) * Float.BYTES + Integer.BYTES + 4 * Integer.BYTES));
//        List<UniformInfo> uniformInfos = new LinkedList<>();
//        //uniform
//        uniformInfos.add(new UniformInfo("model", UniformInfo.ValueType.MAT4F));
//        uniformInfos.add(new UniformInfo("view", UniformInfo.ValueType.MAT4F));
//        uniformInfos.add(new UniformInfo("projection", UniformInfo.ValueType.MAT4F));
//        uniformInfos.add(new UniformInfo("bones", UniformInfo.ValueType.MAT4F));
//        uniformInfos.add(new UniformInfo("diffuseTexture", UniformInfo.ValueType.INT1));
//        uniformInfos.add(new UniformInfo("envTexture", UniformInfo.ValueType.INT1));
//        uniformInfos.add(new UniformInfo("viewPos", UniformInfo.ValueType.FLOAT3));
//        val shaderOpt = Shader.create(vertexShaderStr, fragmentShaderStr, vertexAttributePointers, uniformInfos);
//        if (shaderOpt.isPresent()) {
//            shader = shaderOpt.get();
//        } else {
//            throw new RuntimeException("无法创建PMX模型着色器程序");
//        }
//    }
//
//    private static BufferedImage convert(BufferedImage input) {
//        BufferedImage output = new BufferedImage(input.getWidth(), input.getHeight(), BufferedImage.TYPE_4BYTE_ABGR);
//        Graphics graphics = output.getGraphics();
//        graphics.drawImage(input, 0, 0, null);
//        graphics.dispose();
//        return output;
//    }
//
//    public PMXModel(Camera camera, PMXStruct modelStruct) throws IOException {
//        this.camera = camera;
//        //载入纹理
//        val textureDataList = modelStruct.getTextureDataList();
//        Map<Integer, Texture> textureMap = new HashMap<>(textureDataList.size());
//        val textureManager = TextureManagerImpl.INSTANCE;
//        for (int i = 0; i < textureDataList.size(); i++) {
//            PMXTextureData textureData = textureDataList.get(i);
//            val path = textureData.getPath();
////            val file = new File("D:\\BaiduNetdiskDownload\\东方Project\\魂魄妖夢\\魂魄妖夢Ver1.11(水着)\\Model\\魂魄妖夢Ver1.11(水着)\\" + path.replace("tga", "png"));
//            val file = new File("D:\\Programs\\MMD\\models\\ちびAppearance_Miku_Ver1_51 - 红色小九尾\\" + path.replace("tga", "png"));
//            BufferedImage image = ImageIO.read(file);
//            image = convert(image);
//            val texture = textureManager.loadTexture(image);
//            textureMap.put(i, texture);
//        }
//        //检查骨骼数量
//        if (modelStruct.getBoneDataList().size() > 253) {
//            throw new RuntimeException("骨骼数量超限");
//        }
//
//        //顶点数据 position normal texCoords weightDeformType boneIndices boneWeights
//        val vertexDataList = modelStruct.getVertexDataList();
//        ByteBuffer vertexBuffer = BufferUtils.createByteBuffer(vertexDataList.size() * 4 * 17);
//        for (PMXVertexData vertexData : vertexDataList) {
//            val position = vertexData.getPosition();
//            val normal = vertexData.getNormal();
//            val uv = vertexData.getUv();
//            //position
//            vertexBuffer.putFloat(position.x);
//            vertexBuffer.putFloat(position.y);
//            vertexBuffer.putFloat(position.z);
//            //normal
//            vertexBuffer.putFloat(normal.x);
//            vertexBuffer.putFloat(normal.y);
//            vertexBuffer.putFloat(normal.z);
//            //texCoords
//            vertexBuffer.putFloat(uv.x);
//            vertexBuffer.putFloat(uv.y);
//            //weightDeformType
//            val boneDeform = vertexData.getBoneDeform();
//            val deformType = boneDeform.getType();
//            vertexBuffer.putInt(deformType.ordinal());
//            switch (deformType) {
//                case BDEF1: {
//                    BDEF bdef = (BDEF) boneDeform;
//                    //boneIndices
//                    vertexBuffer.putInt(bdef.getIndex()[0]);
//                    vertexBuffer.putInt(0);
//                    vertexBuffer.putInt(0);
//                    vertexBuffer.putInt(0);
//                    //boneWeights
//                    vertexBuffer.putFloat(0);
//                    vertexBuffer.putFloat(0);
//                    vertexBuffer.putFloat(0);
//                    vertexBuffer.putFloat(0);
//                }
//                break;
//                case BDEF2: {
//                    BDEF bdef = (BDEF) boneDeform;
//                    //boneIndices
//                    vertexBuffer.putInt(bdef.getIndex()[0]);
//                    vertexBuffer.putInt(bdef.getIndex()[1]);
//                    vertexBuffer.putInt(0);
//                    vertexBuffer.putInt(0);
//                    //boneWeights
//                    vertexBuffer.putFloat(bdef.getBoneWeights()[0]);
//                    vertexBuffer.putFloat(0);
//                    vertexBuffer.putFloat(0);
//                    vertexBuffer.putFloat(0);
//                }
//                break;
//                case BDEF4: {
//                    BDEF bdef = (BDEF) boneDeform;
//                    //boneIndices
//                    vertexBuffer.putInt(bdef.getIndex()[0]);
//                    vertexBuffer.putInt(bdef.getIndex()[1]);
//                    vertexBuffer.putInt(bdef.getIndex()[2]);
//                    vertexBuffer.putInt(bdef.getIndex()[3]);
//                    //boneWeights
//                    vertexBuffer.putFloat(bdef.getBoneWeights()[0]);
//                    vertexBuffer.putFloat(bdef.getBoneWeights()[1]);
//                    vertexBuffer.putFloat(bdef.getBoneWeights()[2]);
//                    vertexBuffer.putFloat(bdef.getBoneWeights()[3]);
//                }
//                break;
//                case SDEF:
//                    SDEF sdef = (SDEF) boneDeform;
//                    //boneIndices
//                    vertexBuffer.putInt(sdef.getIndex1());
//                    vertexBuffer.putInt(sdef.getIndex2());
//                    vertexBuffer.putInt(0);
//                    vertexBuffer.putInt(0);
//                    //boneWeights
//                    vertexBuffer.putFloat(sdef.getBone1Wight());
//                    vertexBuffer.putFloat(0);
//                    vertexBuffer.putFloat(0);
//                    vertexBuffer.putFloat(0);
//                    break;
//                case QDEF:
//                    QDEF qdef = (QDEF) boneDeform;
//                    //boneIndices
//                    vertexBuffer.putInt(qdef.getIndex()[0]);
//                    vertexBuffer.putInt(qdef.getIndex()[1]);
//                    vertexBuffer.putInt(qdef.getIndex()[2]);
//                    vertexBuffer.putInt(qdef.getIndex()[3]);
//                    //boneWeights
//                    vertexBuffer.putFloat(qdef.getBoneWeights()[0]);
//                    vertexBuffer.putFloat(qdef.getBoneWeights()[1]);
//                    vertexBuffer.putFloat(qdef.getBoneWeights()[2]);
//                    vertexBuffer.putFloat(qdef.getBoneWeights()[3]);
//                    break;
//            }
//
//        }
//        vertexBuffer.flip();
//        //从顶点数据创建VBO
//        val vbo = new VertexBufferObject(GL_ARRAY_BUFFER, GL_STATIC_DRAW);
//        val vertexBufferData = new BufferData(vertexBuffer);
//        vertexBufferData.upload(vbo);
//        //面和材质数据
//        val surfaceDataList = modelStruct.getSurfaceDataList();
//        val materialDataList = modelStruct.getMaterialDataList();
//        int surfaceIndex = 0;
//        for (PMXMaterialData materialData : materialDataList) {
//            //创建PartRender
//            val surfaceCount = materialData.getSurfaceCount() / 3;
//            //创建EBO
//            IntBuffer partSurfaceBuffer = BufferUtils.createIntBuffer(surfaceCount * 3);
//            for (int i = 0; i < surfaceCount; i++) {
//                val surfaceData = surfaceDataList.get(surfaceIndex + i);
//                val vertexA = surfaceData.getVertexA();
//                val vertexB = surfaceData.getVertexB();
//                val vertexC = surfaceData.getVertexC();
//                partSurfaceBuffer.put(vertexA);
//                partSurfaceBuffer.put(vertexB);
//                partSurfaceBuffer.put(vertexC);
//            }
//            partSurfaceBuffer.flip();
//            val surfaceBufferData = new BufferData(partSurfaceBuffer);
//            val ebo = new VertexBufferObject(GL_ELEMENT_ARRAY_BUFFER, GL_STATIC_DRAW);
//            surfaceBufferData.upload(ebo);
//            //Buffers
//            VertexArray vertexArray = new VertexArray();
//            val vertexBufferObjectList = vertexArray.getVertexBufferObjectList();
//            vertexBufferObjectList.add(ebo);
//            vertexBufferObjectList.add(vbo);
//            //vao
//            VertexArrayObject vao = new VertexArrayObject(shader, vertexArray);
//            //renderer
//            val renderer = new BakedElement(shader, vao, GL_TRIANGLES, surfaceCount * 3, GL_UNSIGNED_INT, 0);
//            //初始化着色器参数
//            val modelMat = new Matrix4f().translate(0, -8, 0).rotate((float) Math.toRadians(180), 0.0f, 1.0f, 0.0f);
//            //TODO 修改屏幕比例
//            val projectionMat = new Matrix4f().perspective((float) Math.toRadians(45.0f), (float) 1600 / (float) 900,
//                    0.1f, 100.0f);
//            val shaderParams = new ShaderParams();
//            val diffuseTexture = textureMap.get(materialData.getTextureIndex());
//            if (diffuseTexture != null)
//                shaderParams.setTexture(0, diffuseTexture);
//            if (materialData.getEnvironmentBlendMode() != PMXMaterialData.EnvironmentBlendMode.DISABLED) {
//                val envTexture = textureMap.get(materialData.getEnvironmentIndex());
//                if (envTexture != null)
//                    shaderParams.setTexture(1, envTexture);
//            }
//            //uniform
//            shaderParams.putShaderUniform(new ShaderUniformMat(new UniformInfo("model", UniformInfo.ValueType.MAT4F)).put(modelMat).setTranspose(false));
//            shaderParams.putShaderUniform(new ShaderUniformMat(new UniformInfo("view", UniformInfo.ValueType.MAT4F)).put(camera.getView()).setTranspose(false));
//            shaderParams.putShaderUniform(new ShaderUniformMat(new UniformInfo("projection", UniformInfo.ValueType.MAT4F)).put(projectionMat).setTranspose(false));
//            shaderParams.putShaderUniform(new ShaderUniformInt(new UniformInfo("diffuseTexture", UniformInfo.ValueType.INT1)).put(0));
//            shaderParams.putShaderUniform(new ShaderUniformInt(new UniformInfo("envTexture", UniformInfo.ValueType.INT1)).put(1));
//            shaderParams.putShaderUniform(new ShaderUniformFloat(new UniformInfo("viewPos", UniformInfo.ValueType.FLOAT3)).put(camera.getPos()));
//            val bonesUniform = new ShaderUniformMat(new UniformInfo("bones", UniformInfo.ValueType.MAT4F), 253);
//            //初始化为单位矩阵
//            for (int i = 0; i < 253; i++) {
//                bonesUniform.put(new Matrix4f());
//            }
//            shaderParams.putShaderUniform(bonesUniform.setTranspose(false));
//            partRenderers.put(renderer, shaderParams);
//            surfaceIndex += surfaceCount;
//        }
//    }
//
//    public void render() {
//        glEnable(GL_DEPTH_TEST);
//        glEnable(GL_MULTISAMPLE);
//        glEnable(GL_CULL_FACE);
////        glCullFace(GL_FRONT);
////        glFrontFace(GL_CW);
//        partRenderers.forEach(((bakedPart, shaderParams) -> {
//            shaderParams.getUniformAsMat("view").clear().put(camera.getView());
//            shaderParams.getUniformAsFloat("viewPos").clear().put(camera.getPos());
//            bakedPart.render(shaderParams);
//        }));
//    }
//}
