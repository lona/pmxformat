package com.lonacloud.modelloader.pmx;

import com.lonacloud.modelloader.pmx.data.*;
import com.lonacloud.modelloader.pmx.exception.PMXLoadException;
import com.lonacloud.modelloader.pmx.util.PMXTypeReader;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.val;
import lombok.var;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author lona, 1307693959@qq.com
 * @version 1.0
 **/
@AllArgsConstructor
@Getter
public class PMXStruct {
    public final PMXHeader header;
    public final List<PMXVertexData> vertexDataList;
    public final List<PMXSurfaceData> surfaceDataList;
    public final List<PMXTextureData> textureDataList;
    public final List<PMXMaterialData> materialDataList;
    public final List<PMXBoneData> boneDataList;
    public final List<PMXMorphData> morphDataList;
    public final List<PMXDisplayFrameData> displayFrameDataList;
    public final List<PMXRigidBodyData> rigidBodyDataList;
    public final List<PMXJointData> jointDataList;
    public final List<PMXSoftBodyData> softBodyDataList;

    public static Optional<PMXStruct> load(InputStream inputStream) throws IOException, PMXLoadException {
        val headerOpt = PMXHeader.load(inputStream);
        if (!headerOpt.isPresent()) return Optional.empty();
        val header = headerOpt.get();
        val vertexCount = PMXTypeReader.readInt(inputStream);
        val vertexDataList = readVertexDataList(vertexCount, header, inputStream);
        val surfaceCount = PMXTypeReader.readInt(inputStream);
        val surfaceDataCount = surfaceCount / 3;
        val surfaceDataList = readSurfaceDataList(surfaceDataCount, header, inputStream);
        val textureCount = PMXTypeReader.readInt(inputStream);
        val textureList = readTextureDataList(textureCount, header, inputStream);
        val materialCount = PMXTypeReader.readInt(inputStream);
        val materialList = readMaterialDataList(materialCount, header, inputStream);
        val boneCount = PMXTypeReader.readInt(inputStream);
        val boneDataList = readBoneDataList(boneCount, header, inputStream);
        val morphCount = PMXTypeReader.readInt(inputStream);
        val morphDataList = readMorphDataList(morphCount, header, inputStream);
        val displayFrameCount = PMXTypeReader.readInt(inputStream);
        val displayFrameDataList = readDisplayFrameDataList(displayFrameCount, header, inputStream);
        val rigidBodyCount = PMXTypeReader.readInt(inputStream);
        val readRigidBodyDataList = readRigidBodyDataList(rigidBodyCount, header, inputStream);
        val jointCount = PMXTypeReader.readInt(inputStream);
        val jointDataList = readJointDataList(jointCount, header, inputStream);
        List<PMXSoftBodyData> softBodyDataList = new ArrayList<>(0);
        if (header.getVersion() == 2.1f) {
            val softBodyCount = PMXTypeReader.readInt(inputStream);
            softBodyDataList = readSoftBodyDataList(softBodyCount, header, inputStream);
        }
        return Optional.of(new PMXStruct(header, vertexDataList, surfaceDataList,
                textureList, materialList, boneDataList, morphDataList, displayFrameDataList, readRigidBodyDataList, jointDataList, softBodyDataList));
    }

    private static List<PMXVertexData> readVertexDataList(final int vertexCount, PMXHeader header, InputStream inputStream) throws IOException, PMXLoadException {
        List<PMXVertexData> vertexDataList = new ArrayList<>(vertexCount);
        for (var i = 0; i < vertexCount; i++) {
            val vertexData = PMXVertexData.load(header, inputStream);
            vertexDataList.add(vertexData);
        }
        return vertexDataList;
    }

    private static List<PMXSurfaceData> readSurfaceDataList(final int surfaceDataCount, PMXHeader header, InputStream inputStream) throws PMXLoadException, IOException {
        List<PMXSurfaceData> surfaceDataList = new ArrayList<>(surfaceDataCount);
        for (var i = 0; i < surfaceDataCount; i++) {
            val surfaceData = PMXSurfaceData.load(header, inputStream);
            surfaceDataList.add(surfaceData);
        }
        return surfaceDataList;
    }

    private static List<PMXTextureData> readTextureDataList(final int textureCount, PMXHeader header, InputStream inputStream) throws PMXLoadException, IOException {
        List<PMXTextureData> textureDataList = new ArrayList<>(textureCount);
        for (var i = 0; i < textureCount; i++) {
            val data = PMXTextureData.load(header, inputStream);
            textureDataList.add(data);
        }
        return textureDataList;
    }

    private static List<PMXMaterialData> readMaterialDataList(final int materialCount, PMXHeader header, InputStream inputStream) throws PMXLoadException, IOException {
        List<PMXMaterialData> materialDataList = new ArrayList<>(materialCount);
        for (var i = 0; i < materialCount; i++) {
            val materialData = PMXMaterialData.load(header, inputStream);
            materialDataList.add(materialData);
        }
        return materialDataList;
    }


    private static List<PMXBoneData> readBoneDataList(final int boneCount, PMXHeader header, InputStream inputStream) throws PMXLoadException, IOException {
        List<PMXBoneData> list = new ArrayList<>(boneCount);
        for (var i = 0; i < boneCount; i++) {
            val data = PMXBoneData.load(header, inputStream);
            list.add(data);
        }
        return list;
    }

    private static List<PMXMorphData> readMorphDataList(final int morphCount, PMXHeader header, InputStream inputStream) throws PMXLoadException, IOException {
        List<PMXMorphData> list = new ArrayList<>(morphCount);
        for (var i = 0; i < morphCount; i++) {
            val data = PMXMorphData.load(header, inputStream);
            list.add(data);
        }
        return list;
    }

    private static List<PMXDisplayFrameData> readDisplayFrameDataList(final int displayFrameCount, PMXHeader header, InputStream inputStream) throws PMXLoadException, IOException {
        List<PMXDisplayFrameData> list = new ArrayList<>(displayFrameCount);
        for (var i = 0; i < displayFrameCount; i++) {
            val data = PMXDisplayFrameData.load(header, inputStream);
            list.add(data);
        }
        return list;
    }

    private static List<PMXRigidBodyData> readRigidBodyDataList(final int rigidBodyCount, PMXHeader header, InputStream inputStream) throws PMXLoadException, IOException {
        List<PMXRigidBodyData> list = new ArrayList<>(rigidBodyCount);
        for (var i = 0; i < rigidBodyCount; i++) {
            val data = PMXRigidBodyData.load(header, inputStream);
            list.add(data);
        }
        return list;
    }

    private static List<PMXJointData> readJointDataList(final int jointCount, PMXHeader header, InputStream inputStream) throws PMXLoadException, IOException {
        List<PMXJointData> list = new ArrayList<>(jointCount);
        for (var i = 0; i < jointCount; i++) {
            val data = PMXJointData.load(header, inputStream);
            list.add(data);
        }
        return list;
    }

    private static List<PMXSoftBodyData> readSoftBodyDataList(final int softBodyCount, PMXHeader header, InputStream inputStream) throws PMXLoadException, IOException {
        List<PMXSoftBodyData> list = new ArrayList<>(softBodyCount);
        for (var i = 0; i < softBodyCount; i++) {
            val data = PMXSoftBodyData.load(header, inputStream);
            list.add(data);
        }
        return list;
    }
}
