package com.lonacloud.modelloader.pmx.data;

import com.lonacloud.modelloader.pmx.exception.PMXLoadException;
import com.lonacloud.modelloader.pmx.util.BitsUtil;
import com.lonacloud.modelloader.pmx.util.PMXIndexReader;
import com.lonacloud.modelloader.pmx.util.PMXTypeReader;
import lombok.Data;
import lombok.val;
import org.joml.Vector3f;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author lona, 1307693959@qq.com
 * @version 1.0
 **/
@Data
public class PMXBoneData {
    public final String boneNameLocal,//Handy name for the bone (Usually Japanese)
            boneNameUniversal;//Handy name for the bone (Usually English)
    public final Vector3f position;//The local translation of the bone
    public final int parentBoneIndex;//
    public final int layer;//Deformation hierarchy
    public final BoneFlags boneFlags;//See Bone Flags
    public final Vector3f tailPositionVec3;//If indexed tail position flag is set then this is a bone index
    public final int tailPositionIndex;//Vec3和Index类型只能二选一
    public final InheritBone inheritBone;//Used if either of the inherit flags are set. See Inherit Bone
    public final BoneFixedAxis fixedAxis;//Used if fixed axis flag is set. See Bone Fixed Axis
    public final BoneLocalCoOrdinate localCoOrdinate;//Used if local co-ordinate flag is set. See Bone Local Co-ordinate
    public final BoneExternalParent externalParent;//Used if external parent deform flag is set. See Bone External Parent
    public final BoneIK ik;//Used if IK flag is set. See Bone IK

    @Data
    public static class BoneFlags {
        private final boolean
                indexedTailPosition,//0 Is the tail position a vec3 or bone index
                rotatable,//1 Enables rotation
                translatable,//2 Enables translation (shear)
                isVisible,//3 ???
                enabled,//4 ???
                inversekinematics,//5 Use inverse kinematics (physics)
                inheritRotation,//8 Rotation inherits from another bone
                inheritTranslation,//9 Translation inherits from another bone
                fixedAxis,//10 The bone's shaft is fixed in a direction
                localCoOrdinate,//11 ???
                physicsAfterDeform,//12 ???
                externalParentDeform;//13 ???

        public static BoneFlags load(InputStream inputStream) throws IOException {
            final byte[] buf = new byte[2];
            inputStream.read(buf);
            boolean indexedTailPosition = BitsUtil.bitCheck(buf[0], 0),
                    rotatable = BitsUtil.bitCheck(buf[0], 1),//Enables rotation
                    translatable = BitsUtil.bitCheck(buf[0], 2),//Enables translation (shear)
                    isVisible = BitsUtil.bitCheck(buf[0], 3),//???
                    enabled = BitsUtil.bitCheck(buf[0], 4),//???
                    inversekinematics = BitsUtil.bitCheck(buf[0], 5),//Use inverse kinematics (physics)
                    inheritRotation = BitsUtil.bitCheck(buf[1], 0),//Rotation inherits from another bone
                    inheritTranslation = BitsUtil.bitCheck(buf[1], 1),//Translation inherits from another bone
                    fixedAxis = BitsUtil.bitCheck(buf[1], 2),//The bone's shaft is fixed in a direction
                    localCoOrdinate = BitsUtil.bitCheck(buf[1], 3),//???
                    physicsAfterDeform = BitsUtil.bitCheck(buf[1], 4),//???
                    externalParentDeform = BitsUtil.bitCheck(buf[1], 5);//???
            return new BoneFlags(
                    indexedTailPosition,
                    rotatable,
                    translatable,
                    isVisible,
                    enabled,
                    inversekinematics,
                    inheritRotation,
                    inheritTranslation,
                    fixedAxis,
                    localCoOrdinate,
                    physicsAfterDeform,
                    externalParentDeform);
        }
    }

    @Data
    public static class InheritBone {
        private final int parentIndex;
        private final float parentInfluence;

        public static InheritBone load(PMXHeader header, InputStream inputStream) throws IOException, PMXLoadException {
            final int parentIndex = PMXIndexReader.readIndex(header.getGlobal().boneIndexSize, inputStream);
            final float parentInfluence = PMXTypeReader.readFloat(inputStream);
            return new InheritBone(parentIndex, parentInfluence);
        }
    }

    @Data
    public static class BoneFixedAxis {
        private final Vector3f axisDirection;

        public static BoneFixedAxis load(InputStream inputStream) throws IOException {
            val vec3 = PMXTypeReader.readVec3(inputStream);
            return new BoneFixedAxis(vec3);
        }
    }

    @Data
    public static class BoneLocalCoOrdinate {
        private final Vector3f xVector, yVector;

        public static BoneLocalCoOrdinate load(InputStream inputStream) throws IOException {
            final Vector3f xVector = PMXTypeReader.readVec3(inputStream),
                    yVector = PMXTypeReader.readVec3(inputStream);
            return new BoneLocalCoOrdinate(xVector, yVector);
        }
    }

    @Data
    public static class BoneExternalParent {
        private final int parentIndex;

        public static BoneExternalParent load(PMXHeader header, InputStream inputStream) throws IOException, PMXLoadException {
            final int parentIndex = PMXIndexReader.readIndex(header.getGlobal().boneIndexSize, inputStream);
            return new BoneExternalParent(parentIndex);
        }
    }

    @Data
    public static class IKAngleLimit {
        private final Vector3f limitMin, limitMax;

        public static IKAngleLimit load(InputStream inputStream) throws IOException {
            final Vector3f limitMin = PMXTypeReader.readVec3(inputStream),
                    limitMax = PMXTypeReader.readVec3(inputStream);
            return new IKAngleLimit(limitMin, limitMax);
        }
    }

    @Data
    public static class IKLink {
        private final int boneIndex;
        private final boolean hasLimits;//When equal to 1, use angle limits
        private final IKAngleLimit ikAngleLimit;//Used if has limits is 1. See IK Angle Limit

        public static IKLink load(PMXHeader header, InputStream inputStream) throws IOException, PMXLoadException {
            final int boneIndex = PMXIndexReader.readIndex(header.getGlobal().boneIndexSize, inputStream);
            final boolean hasLimits = ((byte) inputStream.read()) == 1;
            IKAngleLimit ikAngleLimit = null;
            if (hasLimits) {
                ikAngleLimit = IKAngleLimit.load(inputStream);
            }
            return new IKLink(boneIndex, hasLimits, ikAngleLimit);
        }
    }

    @Data
    public static class BoneIK {
        private final int targetIndex;
        private final int loopCount;//???
        private final float limitRadian;//???
        private final int linkCount;//How many bones this IK links with
        private final IKLink[] ikLinks;//See IK Link

        public static BoneIK load(PMXHeader header, InputStream inputStream) throws IOException, PMXLoadException {
            final int targetIndex = PMXIndexReader.readIndex(header.getGlobal().boneIndexSize, inputStream);
            final int loopCount = PMXTypeReader.readInt(inputStream);
            final float limitRadian = PMXTypeReader.readFloat(inputStream);
            final int linkCount = PMXTypeReader.readInt(inputStream);
            IKLink[] ikLinks = new IKLink[linkCount];
            for (int i = 0; i < linkCount; i++) {
                val ikLink = IKLink.load(header, inputStream);
                ikLinks[i] = ikLink;
            }
            return new BoneIK(targetIndex, loopCount, limitRadian, linkCount, ikLinks);
        }
    }


    public static PMXBoneData load(PMXHeader header, InputStream inputStream) throws IOException, PMXLoadException {
        val global = header.getGlobal();
        val charset = global.getEncoding();
        val boneIndexSize = global.boneIndexSize;
        val boneNameLocal = PMXTypeReader.readString(charset, inputStream);
        val boneNameUniversal = PMXTypeReader.readString(charset, inputStream);
        val position = PMXTypeReader.readVec3(inputStream);
        val parentBoneIndex = PMXIndexReader.readIndex(boneIndexSize, inputStream);
        val layer = PMXTypeReader.readInt(inputStream);
        val boneFlags = BoneFlags.load(inputStream);
        Vector3f tailPositionVec3 = null;
        int tailPositionIndex = -1;
        if (boneFlags.indexedTailPosition) {
            tailPositionIndex = PMXIndexReader.readIndex(boneIndexSize, inputStream);
        } else {
            tailPositionVec3 = PMXTypeReader.readVec3(inputStream);
        }
        InheritBone inheritBone = (boneFlags.inheritRotation || boneFlags.inheritTranslation) ? InheritBone.load(header, inputStream) : null;
        BoneFixedAxis fixedAxis = boneFlags.fixedAxis ? BoneFixedAxis.load(inputStream) : null;
        BoneLocalCoOrdinate localCoOrdinate = boneFlags.localCoOrdinate ? BoneLocalCoOrdinate.load(inputStream) : null;
        BoneExternalParent externalParent = boneFlags.externalParentDeform ? BoneExternalParent.load(header, inputStream) : null;
        BoneIK ik = boneFlags.inversekinematics ? BoneIK.load(header, inputStream) : null;
        return new PMXBoneData(boneNameLocal, boneNameUniversal, position, parentBoneIndex,
                layer, boneFlags, tailPositionVec3, tailPositionIndex, inheritBone, fixedAxis, localCoOrdinate, externalParent, ik);
    }
}
