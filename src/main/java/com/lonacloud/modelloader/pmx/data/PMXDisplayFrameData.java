package com.lonacloud.modelloader.pmx.data;

import com.lonacloud.modelloader.pmx.exception.PMXLoadException;
import com.lonacloud.modelloader.pmx.util.PMXIndexReader;
import com.lonacloud.modelloader.pmx.util.PMXTypeReader;
import lombok.Data;
import lombok.val;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author lona, 1307693959@qq.com
 * @version 1.0
 **/
@Data
public class PMXDisplayFrameData {
    private final String displayNameLocal,//Handy name for the display (Usually Japanese)
            displayNameUniversal;//Handy name for the display (Usually English)
    private final boolean specialFlag;//false = normal frame, true = special frame
    private final FrameData[] frameData;

    public static PMXDisplayFrameData load(PMXHeader header, InputStream inputStream) throws IOException, PMXLoadException {
        val charset = header.getGlobal().getEncoding();
        final String displayNameLocal = PMXTypeReader.readString(charset, inputStream);
        final String displayNameUniversal = PMXTypeReader.readString(charset, inputStream);
        final boolean specialFlag = PMXTypeReader.readBool(inputStream);
        final int frameCount = PMXTypeReader.readInt(inputStream);
        final FrameData[] frameData = new FrameData[frameCount];
        for (int i = 0; i < frameCount; i++) {
            val data = FrameData.load(header, inputStream);
            frameData[i] = data;
        }
        return new PMXDisplayFrameData(displayNameLocal, displayNameUniversal, specialFlag, frameData);
    }


    public enum FrameType {
        BONE,
        MORPH
    }

    @Data
    public static final class FrameData {
        private final FrameType frameType;
        private final int index;

        public static FrameData load(PMXHeader header, InputStream inputStream) throws IOException, PMXLoadException {
            final byte frameTypeByte = (byte) inputStream.read();
            FrameType frameType;
            try {
                frameType = FrameType.values()[frameTypeByte];
            } catch (IndexOutOfBoundsException e) {
                throw new PMXLoadException("无法识别的FrameType:" + frameTypeByte);
            }
            int index = -1;
            val global = header.getGlobal();
            switch (frameType) {
                case BONE:
                    index = PMXIndexReader.readIndex(global.boneIndexSize, inputStream);
                    break;
                case MORPH:
                    index = PMXIndexReader.readIndex(global.morphIndexSize, inputStream);
                    break;
                default:
                    throw new PMXLoadException("无法解析的FrameType:" + frameType.name());
            }
            return new FrameData(frameType, index);
        }
    }
}
