package com.lonacloud.modelloader.pmx.data;

import com.lonacloud.modelloader.pmx.util.PMXTypeReader;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import lombok.val;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Optional;

/**
 * @author lona, 1307693959@qq.com
 * @version 1.0
 **/
@ToString
@AllArgsConstructor
public class PMXHeader {
    private static final byte[] expectedSignature = new byte[]{0x50, 0x4D, 0x58, 0x20};//Notice the space at the end
    @Getter
    private final float version;//2.0, 2.1
    @Getter
    private final byte[] globals;//Fixed at 8 for PMX 2.0
    @Getter
    private final Global global;
    @Getter
    private final String modelNameLocal,//Name of model
            modelNameUniversal,//Name of model
            commentsLocal,//Additional information (license)
            commentsUniversal;//Additional information (license)

    @ToString
    public static class Global {
        public final byte textEncoding,//0, 1 Byte encoding for the "text" type, 0 = UTF16LE, 1 = UTF8
                additionalVec4Count,//0..4    Additional vec4 values are added to each vertex
                vertexIndexSize,//1, 2, 4     The index type for vertices (See Index Types above)
                textureIndexSize,//1, 2, 4    The index type for textures (See Index Types above)
                materialIndexSize,//1, 2, 4   The index type for materials (See Index Types above)
                boneIndexSize,//1, 2, 4       The index type for bones (See Index Types above)
                morphIndexSize,//1, 2, 4      The index type for morphs (See Index Types above)
                rigidbodyIndexSize;//1, 2, 4  The index type for rigid bodies (See Index Types above)
        @Getter
        private final Charset encoding;

        public Global(final byte[] buf) {
            textEncoding = buf[0];
            additionalVec4Count = buf[1];
            vertexIndexSize = buf[2];
            textureIndexSize = buf[3];
            materialIndexSize = buf[4];
            boneIndexSize = buf[5];
            morphIndexSize = buf[6];
            rigidbodyIndexSize = buf[7];
            encoding = textEncoding == 0 ? StandardCharsets.UTF_16LE : StandardCharsets.UTF_8;
        }
    }

    public static Optional<PMXHeader> load(InputStream inputStream) throws IOException {
        final byte[] signature = new byte[4];
        inputStream.read(signature);
        if (!Arrays.equals(expectedSignature, signature)) {
            return Optional.empty();
        }
        final float version = PMXTypeReader.readFloat(inputStream);
        final byte globalsCount = (byte) inputStream.read();
        final byte[] globals = new byte[globalsCount];
        inputStream.read(globals);
        final Global global = new Global(globals);
        val encoding = global.getEncoding();
        String modelNameLocal = PMXTypeReader.readString(encoding, inputStream);
        String modelNameUniversal = PMXTypeReader.readString(encoding, inputStream);
        String commentsLocal = PMXTypeReader.readString(encoding, inputStream);
        String commentsUniversal = PMXTypeReader.readString(encoding, inputStream);
        return Optional.of(new PMXHeader(version, globals, global, modelNameLocal, modelNameUniversal,
                commentsLocal, commentsUniversal));
    }

}
