package com.lonacloud.modelloader.pmx.data;

import com.lonacloud.modelloader.pmx.exception.PMXLoadException;
import com.lonacloud.modelloader.pmx.util.PMXIndexReader;
import com.lonacloud.modelloader.pmx.util.PMXTypeReader;
import lombok.Data;
import lombok.val;
import org.joml.Vector3f;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author lona, 1307693959@qq.com
 * @version 1.0
 **/
@Data
public class PMXJointData {
    private final String jointNameLocal,//Handy name for the rigid body (Usually Japanese)
            jointNameUniversal;//Handy name for the rigid body (Usually English)
    private final JointType type;
    private final int rigidBodyIndexA, rigidBodyIndexB;
    private final Vector3f position,
            rotation,//Rotation angle radian
            positionMinimum, positionMaximum,
            rotationMinimum, rotationMaximum,
            positionSpring, rotationSpring;

    public enum JointType {
        SPRING_6DOF,
        _6DOF,
        P2P,
        CONETWIST,
        SLIDER,
        HINGE
    }

    public static PMXJointData load(PMXHeader header, InputStream inputStream) throws IOException, PMXLoadException {
        val global = header.getGlobal();
        val charset = global.getEncoding();
        final String jointNameLocal = PMXTypeReader.readString(charset, inputStream),
                jointNameUniversal = PMXTypeReader.readString(charset, inputStream);
        final byte jointTypeByte = (byte) inputStream.read();
        JointType type;
        try {
            type = JointType.values()[jointTypeByte];
        } catch (IndexOutOfBoundsException e) {
            throw new PMXLoadException("无法识别的JointType:" + jointTypeByte);
        }
        final int rigidBodyIndexA = PMXIndexReader.readIndex(global.rigidbodyIndexSize, inputStream),
                rigidBodyIndexB = PMXIndexReader.readIndex(global.rigidbodyIndexSize, inputStream);
        final Vector3f position = PMXTypeReader.readVec3(inputStream),
                rotation = PMXTypeReader.readVec3(inputStream),
                positionMinimum = PMXTypeReader.readVec3(inputStream),
                positionMaximum = PMXTypeReader.readVec3(inputStream),
                rotationMinimum = PMXTypeReader.readVec3(inputStream),
                rotationMaximum = PMXTypeReader.readVec3(inputStream),
                positionSpring = PMXTypeReader.readVec3(inputStream),
                rotationSpring = PMXTypeReader.readVec3(inputStream);
        return new PMXJointData(jointNameLocal, jointNameUniversal, type, rigidBodyIndexA, rigidBodyIndexB,
                position, rotation, positionMinimum, positionMaximum, rotationMinimum, rotationMaximum, positionSpring, rotationSpring);
    }
}
