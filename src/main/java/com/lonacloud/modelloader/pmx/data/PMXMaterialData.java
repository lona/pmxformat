package com.lonacloud.modelloader.pmx.data;

import com.lonacloud.modelloader.pmx.exception.PMXLoadException;
import com.lonacloud.modelloader.pmx.util.BitsUtil;
import com.lonacloud.modelloader.pmx.util.PMXIndexReader;
import com.lonacloud.modelloader.pmx.util.PMXTypeReader;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.val;
import org.joml.Vector3f;
import org.joml.Vector4f;

import java.io.IOException;
import java.io.InputStream;

/**
 * Internally, materials would define the shading for the model when being rendered.
 * PMX is intended for material-based rendering, not physically-based rendering,
 *
 * @author lona, 1307693959@qq.com
 * @version 1.0
 **/
@Data
public class PMXMaterialData {
    public final String materialNameLocal,//Handy name for the material (Usually Japanese)
            materialNameUniversal;//Handy name for the material (Usually English)
    public final Vector4f diffuseColour;//RGBA colour (Alpha will set a semi-transparent material)
    public final Vector3f specularColour;//RGB colour of the reflected light
    public final float specularStrength;//The "size" of the specular highlight
    public final Vector3f ambientColour;//RGB colour of the material shadow (When out of light)
    public final MaterialFlags drawingFlags;//See Material Flags
    public final Vector4f edgeColour;//RGBA colour of the pencil-outline edge (Alpha for semi-transparent)
    public final float edgeScale;//Pencil-outline scale (1.0 should be around 1 pixel)
    public final int textureIndex;//See Index Types, this is from the texture table by default
    public final int environmentIndex;//Same as texture index, but for environment mapping
    public final EnvironmentBlendMode environmentBlendMode;//0 = disabled, 1 = multiply, 2 = additive, 3 = additional vec4*
    public final byte toonReference;//0 = Texture reference, 1 = internal reference
    public final int toonValue;//Behaviour depends on Toon reference value**
    public final String metaData;//This is used for scripting or additional data
    public final int surfaceCount;//How many surfaces this material affects***

    @EqualsAndHashCode
    @Getter
    public static class MaterialFlags {
        private final boolean noCull,//Disables back-face culling
                groundShadow,//Projects a shadow onto the geometry
                drawShadow,//Renders to the shadow map
                receiveShadow,//Receives a shadow from the shadow map
                hasEdge,//Has "pencil" outline
                vertexColour,//Uses additional vec4 1 for vertex colour
                pointDrawing,//Each of the 3 vertices are points
                lineDrawing;//The triangle is rendered as lines

        public MaterialFlags(byte flag) {
            noCull = BitsUtil.bitCheck(flag, 0);
            groundShadow = BitsUtil.bitCheck(flag, 1);
            drawShadow = BitsUtil.bitCheck(flag, 2);
            receiveShadow = BitsUtil.bitCheck(flag, 3);
            hasEdge = BitsUtil.bitCheck(flag, 4);
            vertexColour = BitsUtil.bitCheck(flag, 5);
            pointDrawing = BitsUtil.bitCheck(flag, 6);
            lineDrawing = BitsUtil.bitCheck(flag, 7);
        }

    }

    public enum EnvironmentBlendMode {
        DISABLED,
        MULTIPLY,
        ADDITIVE,
        ADDITIONAL_VEC4
    }

    public static PMXMaterialData load(PMXHeader header, InputStream inputStream) throws IOException, PMXLoadException {
        val global = header.getGlobal();
        val encoding = global.getEncoding();
        val materialNameLocal = PMXTypeReader.readString(encoding, inputStream);
        val materialNameUniversal = PMXTypeReader.readString(encoding, inputStream);
        val diffuseColour = PMXTypeReader.readVec4(inputStream);
        val specularColour = PMXTypeReader.readVec3(inputStream);
        final float specularStrength = PMXTypeReader.readFloat(inputStream);
        val ambientColour = PMXTypeReader.readVec3(inputStream);
        final byte drawingFlagsByte = PMXTypeReader.readFlag(inputStream);
        val drawingFlags = new MaterialFlags(drawingFlagsByte);
        val edgeColour = PMXTypeReader.readVec4(inputStream);
        final float edgeScale = PMXTypeReader.readFloat(inputStream);
        final int textureIndex = PMXIndexReader.readIndex(global.textureIndexSize, inputStream);
        final int environmentIndex = PMXIndexReader.readIndex(global.textureIndexSize, inputStream);
        final byte environmentBlendMode = (byte) inputStream.read();
        final byte toonReference = (byte) inputStream.read();
        final int toonValue = toonReference == 0 ? PMXIndexReader.readIndex(global.materialIndexSize, inputStream) : inputStream.read();
        final String metaData = PMXTypeReader.readString(encoding, inputStream);
        final int surfaceCount = PMXTypeReader.readInt(inputStream);
        try {
            return new PMXMaterialData(materialNameLocal, materialNameUniversal,
                    diffuseColour, specularColour, specularStrength, ambientColour,
                    drawingFlags, edgeColour, edgeScale, textureIndex, environmentIndex, EnvironmentBlendMode.values()[environmentBlendMode],
                    toonReference, toonValue, metaData, surfaceCount);
        } catch (IndexOutOfBoundsException e) {
            throw new PMXLoadException("不可识别的EnvironmentBlendMode:" + environmentBlendMode);
        }
    }
}
