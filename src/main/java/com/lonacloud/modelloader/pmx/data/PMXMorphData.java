package com.lonacloud.modelloader.pmx.data;

import com.lonacloud.modelloader.pmx.exception.PMXLoadException;
import com.lonacloud.modelloader.pmx.util.PMXIndexReader;
import com.lonacloud.modelloader.pmx.util.PMXTypeReader;
import lombok.Data;
import lombok.val;
import org.joml.Vector3f;
import org.joml.Vector4f;

import java.io.IOException;
import java.io.InputStream;
import java.util.function.Function;

/**
 * @author lona, 1307693959@qq.com
 * @version 1.0
 **/
@Data
public class PMXMorphData {
    private final String morphNameLocal,//Handy name for the morph (Usually Japanese)
            morphNameUniversal;//Handy name for the morph (Usually English)
    private final MorphPanelType morphPanelType;
    private final MorphType morphType;
    private final MorphOffsetData[] offsetData;

    public static PMXMorphData load(PMXHeader header, InputStream inputStream) throws IOException, PMXLoadException {
        val charset = header.getGlobal().getEncoding();
        final String morphNameLocal = PMXTypeReader.readString(charset, inputStream);
        final String morphNameUniversal = PMXTypeReader.readString(charset, inputStream);
        final byte morphPanelTypeByte = (byte) inputStream.read();
        MorphPanelType morphPanelType;
        try {
            morphPanelType = MorphPanelType.values()[morphPanelTypeByte];
        } catch (IndexOutOfBoundsException e) {
            throw new PMXLoadException("不可识别的MorphPanelType:" + morphPanelTypeByte);
        }
        final byte morphTypeByte = (byte) inputStream.read();
        MorphType morphType;
        try {
            morphType = MorphType.values()[morphTypeByte];
        } catch (IndexOutOfBoundsException e) {
            throw new PMXLoadException("不可识别的MorphType+" + morphTypeByte);
        }
        final int offsetSize = PMXTypeReader.readInt(inputStream);
        final MorphOffsetData[] offsetData = new MorphOffsetData[offsetSize];
        final Function<InputStream, MorphOffsetData> loadFunc;
        switch (morphType) {
            case GROUP:
                loadFunc = new Function<InputStream, MorphOffsetData>() {
                    @Override
                    public MorphOffsetData apply(InputStream inputStream) {
                        try {
                            return MorphGroupData.load(header, inputStream);
                        } catch (IOException | PMXLoadException e) {
                            return null;
                        }
                    }
                };
                break;
            case VERTEX:
                loadFunc = new Function<InputStream, MorphOffsetData>() {
                    @Override
                    public MorphOffsetData apply(InputStream inputStream) {
                        try {
                            return MorphVertexData.load(header, inputStream);
                        } catch (IOException | PMXLoadException e) {
                            return null;
                        }
                    }
                };
                break;
            case BONE:
                loadFunc = new Function<InputStream, MorphOffsetData>() {
                    @Override
                    public MorphOffsetData apply(InputStream inputStream) {
                        try {
                            return MorphBoneData.load(header, inputStream);
                        } catch (IOException | PMXLoadException e) {
                            return null;
                        }
                    }
                };
                break;
            case UV:
            case UV_EXT1:
            case UV_EXT2:
            case UV_EXT3:
            case UV_EXT4:
                loadFunc = new Function<InputStream, MorphOffsetData>() {
                    @Override
                    public MorphOffsetData apply(InputStream inputStream) {
                        try {
                            return MorphUVData.load(morphType, header, inputStream);
                        } catch (IOException | PMXLoadException e) {
                            return null;
                        }
                    }
                };
                break;
            case MATERIAL:
                loadFunc = new Function<InputStream, MorphOffsetData>() {
                    @Override
                    public MorphOffsetData apply(InputStream inputStream) {
                        try {
                            return MorphMaterialData.load(header, inputStream);
                        } catch (IOException | PMXLoadException e) {
                            return null;
                        }
                    }
                };
                break;
            case FLIP:
                loadFunc = new Function<InputStream, MorphOffsetData>() {
                    @Override
                    public MorphOffsetData apply(InputStream inputStream) {
                        try {
                            return MorphFlipData.load(header, inputStream);
                        } catch (IOException | PMXLoadException e) {
                            return null;
                        }
                    }
                };
                break;
            case IMPULSE:
                loadFunc = new Function<InputStream, MorphOffsetData>() {
                    @Override
                    public MorphOffsetData apply(InputStream inputStream) {
                        try {
                            return MorphImpulseData.load(header, inputStream);
                        } catch (IOException | PMXLoadException e) {
                            return null;
                        }
                    }
                };
                break;
            default:
                throw new PMXLoadException("不可识别的MorphType:" + morphType.name());
        }

        for (int i = 0; i < offsetSize; i++) {
            val morphOffsetData = loadFunc.apply(inputStream);
            offsetData[i] = morphOffsetData;
        }
        return new PMXMorphData(morphNameLocal, morphNameUniversal,
                morphPanelType, morphType, offsetData);
    }

    public enum MorphPanelType {
        HIDDEN,
        EYE_BROWS,//Bottom left
        EYES,//Top left
        MOUTH,//Top right
        OTHER,//Bottom right
    }

    public enum MorphType {
        GROUP,
        VERTEX,
        BONE,
        UV,
        UV_EXT1,
        UV_EXT2,
        UV_EXT3,
        UV_EXT4,
        MATERIAL,
        FLIP,
        IMPULSE
    }

    public interface MorphOffsetData {
        MorphType getType();
    }

    @Data
    public static final class MorphGroupData implements MorphOffsetData {
        private final int morphIndex;
        private final float influence;//Weight of indexed morph

        public static MorphGroupData load(PMXHeader header, InputStream inputStream) throws IOException, PMXLoadException {
            final int morphIndex = PMXIndexReader.readIndex(header.getGlobal().morphIndexSize, inputStream);
            final float influence = PMXTypeReader.readFloat(inputStream);
            return new MorphGroupData(morphIndex, influence);
        }

        @Override
        public MorphType getType() {
            return MorphType.GROUP;
        }
    }

    @Data
    public static final class MorphVertexData implements MorphOffsetData {
        private final int vertexIndex;
        private final Vector3f translation;//Translation to apply to vertex

        public static MorphVertexData load(PMXHeader header, InputStream inputStream) throws IOException, PMXLoadException {
            final int vertexIndex = PMXIndexReader.readVertexIndex(header.getGlobal().vertexIndexSize, inputStream);
            final Vector3f translation = PMXTypeReader.readVec3(inputStream);
            return new MorphVertexData(vertexIndex, translation);
        }

        @Override
        public MorphType getType() {
            return MorphType.VERTEX;
        }
    }

    @Data
    public static final class MorphBoneData implements MorphOffsetData {
        private final int boneIndex;
        private final Vector3f translation;//Translation to apply to bone
        private final Vector4f rotation;//Rotation to apply to bone

        public static MorphBoneData load(PMXHeader header, InputStream inputStream) throws IOException, PMXLoadException {
            final int boneIndex = PMXIndexReader.readIndex(header.getGlobal().boneIndexSize, inputStream);
            final Vector3f translation = PMXTypeReader.readVec3(inputStream);
            final Vector4f rotation = PMXTypeReader.readVec4(inputStream);
            return new MorphBoneData(boneIndex, translation, rotation);
        }

        @Override
        public MorphType getType() {
            return MorphType.BONE;
        }
    }

    @Data
    public static final class MorphUVData implements MorphOffsetData {
        private final MorphType type;
        private final int vertexIndex;
        private final Vector4f floats;//What these do depends on the UV ext

        public static MorphUVData load(MorphType type, PMXHeader header, InputStream inputStream) throws IOException, PMXLoadException {
            final int vertexIndex = PMXIndexReader.readVertexIndex(header.getGlobal().vertexIndexSize, inputStream);
            final Vector4f floats = PMXTypeReader.readVec4(inputStream);
            return new MorphUVData(type, vertexIndex, floats);
        }

        @Override
        public MorphType getType() {
            return type;
        }
    }

    @Data
    public static final class MorphMaterialData implements MorphOffsetData {
        private final int materialIndex;
        private final byte unknown;
        private final Vector4f diffuse;
        private final Vector3f specular;
        private final float specularity;
        private final Vector3f ambient;
        private final Vector4f edgeColour;
        private final float edgeSize;
        private final Vector4f textureTint;
        private final Vector4f environmentTint;
        private final Vector4f toonTint;

        public static MorphMaterialData load(PMXHeader header, InputStream inputStream) throws IOException, PMXLoadException {
            final int materialIndex = PMXIndexReader.readIndex(header.getGlobal().materialIndexSize, inputStream);
            final byte unknown = (byte) inputStream.read();
            final Vector4f diffuse = PMXTypeReader.readVec4(inputStream);
            final Vector3f specular = PMXTypeReader.readVec3(inputStream);
            final float specularity = PMXTypeReader.readFloat(inputStream);
            final Vector3f ambient = PMXTypeReader.readVec3(inputStream);
            final Vector4f edgeColour = PMXTypeReader.readVec4(inputStream);
            final float edgeSize = PMXTypeReader.readFloat(inputStream);
            final Vector4f textureTint = PMXTypeReader.readVec4(inputStream);
            final Vector4f environmentTint = PMXTypeReader.readVec4(inputStream);
            final Vector4f toonTint = PMXTypeReader.readVec4(inputStream);
            return new MorphMaterialData(materialIndex, unknown, diffuse, specular, specularity, ambient,
                    edgeColour, edgeSize, textureTint, environmentTint, toonTint);
        }

        @Override
        public MorphType getType() {
            return MorphType.MATERIAL;
        }
    }

    @Data
    public static final class MorphFlipData implements MorphOffsetData {
        private final int morphIndex;
        private final float influence;//Weight of indexed morph

        public static MorphFlipData load(PMXHeader header, InputStream inputStream) throws IOException, PMXLoadException {
            final int morphIndex = PMXIndexReader.readIndex(header.getGlobal().morphIndexSize, inputStream);
            final float influence = PMXTypeReader.readFloat(inputStream);
            return new MorphFlipData(morphIndex, influence);
        }

        @Override
        public MorphType getType() {
            return MorphType.FLIP;
        }
    }

    @Data
    public static final class MorphImpulseData implements MorphOffsetData {
        private final int rigidBodyIndex;
        private final byte localFlag;
        private final Vector3f movementSpeed;
        private final Vector3f rotationTorque;

        public static MorphImpulseData load(PMXHeader header, InputStream inputStream) throws IOException, PMXLoadException {
            final int rigidBodyIndex = PMXIndexReader.readIndex(header.getGlobal().rigidbodyIndexSize, inputStream);
            final byte localFlag = (byte) inputStream.read();
            final Vector3f movementSpeed = PMXTypeReader.readVec3(inputStream);
            final Vector3f rotationTorque = PMXTypeReader.readVec3(inputStream);
            return new MorphImpulseData(rigidBodyIndex, localFlag, movementSpeed, rotationTorque);
        }

        @Override
        public MorphType getType() {
            return MorphType.IMPULSE;
        }
    }
}