package com.lonacloud.modelloader.pmx.data;

import com.lonacloud.modelloader.pmx.exception.PMXLoadException;
import com.lonacloud.modelloader.pmx.util.PMXIndexReader;
import com.lonacloud.modelloader.pmx.util.PMXTypeReader;
import lombok.Data;
import lombok.val;
import org.joml.Vector3f;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author lona, 1307693959@qq.com
 * @version 1.0
 **/
@Data
public class PMXRigidBodyData {
    private final String rigidBodyNameLocal,//Handy name for the rigid body (Usually Japanese)
            rigidBodyNameUniversal;//Handy name for the rigid body (Usually English)
    private final int relatedBoneIndex;
    private final byte groupId;
    private final short nonCollisionGroup;//Non collision mask
    private final ShapeType shapeType;//See Shape Type
    private final Vector3f shapeSize;//XYZ bounds of the shape
    private final Vector3f shapePosition;//XYZ shape location
    private final Vector3f shapeRotation;//XYZ shape rotation angles in radians
    private final float mass, moveAttenuation, rotationDamping, repulsion, frictionForce;
    private final PhysicsMode physicsMode;

    public enum ShapeType {
        SPHERE,
        BOX,
        CAPSULE
    }

    public enum PhysicsMode {
        FOLLOW_BONE,//Rigid body sticks to bone
        PHYSICS,//Rigid body uses gravity
        PHYSICS_AND_BONE//Rigid body uses gravity pivoted to bone
    }

    public static PMXRigidBodyData load(PMXHeader header, InputStream inputStream) throws IOException, PMXLoadException {
        val global = header.getGlobal();
        val charset = global.getEncoding();
        final String rigidBodyNameLocal = PMXTypeReader.readString(charset, inputStream);
        final String rigidBodyNameUniversal = PMXTypeReader.readString(charset, inputStream);
        final int relatedBoneIndex = PMXIndexReader.readIndex(global.boneIndexSize,inputStream);
        final byte groupId = (byte) inputStream.read();
        final short nonCollisionGroup = PMXTypeReader.readShort(inputStream);
        final byte shapeTypeByte = (byte) inputStream.read();
        ShapeType shapeType;
        try {
            shapeType = ShapeType.values()[shapeTypeByte];
        } catch (IndexOutOfBoundsException e) {
            throw new PMXLoadException("无法识别的ShapeType:" + shapeTypeByte);
        }
        final Vector3f shapeSize = PMXTypeReader.readVec3(inputStream);//XYZ bounds of the shape
        final Vector3f shapePosition = PMXTypeReader.readVec3(inputStream);//XYZ shape location
        final Vector3f shapeRotation = PMXTypeReader.readVec3(inputStream);//XYZ shape rotation angles in radians
        final float mass = PMXTypeReader.readFloat(inputStream),
                moveAttenuation = PMXTypeReader.readFloat(inputStream),
                rotationDamping = PMXTypeReader.readFloat(inputStream),
                repulsion = PMXTypeReader.readFloat(inputStream),
                frictionForce = PMXTypeReader.readFloat(inputStream);
        final byte physicsModeByte = (byte) inputStream.read();
        PhysicsMode physicsMode;
        try {
            physicsMode = PhysicsMode.values()[physicsModeByte];
        } catch (IndexOutOfBoundsException e) {
            throw new PMXLoadException("无法识别的PhysicsMode:" + physicsModeByte);
        }
        return new PMXRigidBodyData(rigidBodyNameLocal, rigidBodyNameUniversal, relatedBoneIndex, groupId,
                nonCollisionGroup, shapeType, shapeSize, shapePosition, shapeRotation, mass, moveAttenuation, rotationDamping,
                repulsion, frictionForce, physicsMode);
    }
}
