package com.lonacloud.modelloader.pmx.data;

import com.lonacloud.modelloader.pmx.exception.PMXLoadException;
import com.lonacloud.modelloader.pmx.util.BitsUtil;
import com.lonacloud.modelloader.pmx.util.PMXIndexReader;
import com.lonacloud.modelloader.pmx.util.PMXTypeReader;
import lombok.*;

import java.io.IOException;
import java.io.InputStream;

/**
 * Soft bodies are based on Bullet Physics and are introduced with PMX 2.1
 *
 * @author lona, 1307693959@qq.com
 * @version 1.0
 **/
@Data
public class PMXSoftBodyData {
    private final String softBodyNameLocal,//Handy name for the soft body (Usually Japanese)
            softBodyNameUniversal;//Handy name for the soft body (Usually English)
    private final ShapeType shapeType;//See Shape Types
    private final int materialIndex;//See Index Types
    private final byte group;//Group id
    private final short noCollisionMask;//Non collision mask
    private final Flags flags;//See Flags
    private final int blinkCreateDistance;//???
    private final int numberOfClusters;//???
    private final float totalMass;
    private final float collisionMargin;
    private final AerodynamicsModel aerodynamicsModel;//See Aerodynamics Models
    private final float configVCF,
            configDP,
            configDG,
            configLF,
            configPR,
            configVC,
            configDF,
            configMT,
            configCHR,
            configKHR,
            configSHR,
            configAHR,
            clusterSRHR_CL,
            clusterSKHR_CL,
            clusterSSHR_CL,
            clusterSR_SPLT_CL,
            clusterSK_SPLT_CL,
            clusterSS_SPLT_CL;

    private final int interationV_IT,
            interationP_IT,
            interationD_IT,
            interationC_IT,
            materialLST,
            materialAST,
            materialVST;
    private final AnchorRigidBody[] anchorRigidBodies;
    private final int vertexPins[];

    public static PMXSoftBodyData load(PMXHeader header, InputStream inputStream) throws IOException, PMXLoadException {
        val global = header.getGlobal();
        val charset = global.getEncoding();
        final String softBodyNameLocal = PMXTypeReader.readString(charset, inputStream);
        final String softBodyNameUniversal = PMXTypeReader.readString(charset, inputStream);
        final byte shapeTypeByte = (byte) inputStream.read();
        ShapeType shapeType;
        try {
            shapeType = ShapeType.values()[shapeTypeByte];
        } catch (IndexOutOfBoundsException e) {
            throw new PMXLoadException("不可识别的ShapType:" + shapeTypeByte);
        }
        final int materialIndex = PMXIndexReader.readIndex(global.morphIndexSize, inputStream);
        final byte group = (byte) inputStream.read();
        final short noCollisionMask = PMXTypeReader.readShort(inputStream);
        final Flags flags = new Flags((byte) inputStream.read());
        final int bLinkCreateDistance = PMXTypeReader.readInt(inputStream);
        final int numberOfClusters = PMXTypeReader.readInt(inputStream);
        final float totalMass = PMXTypeReader.readFloat(inputStream);
        final float collisionMargin = PMXTypeReader.readFloat(inputStream);
        final int aerodynamicsModelInt = PMXTypeReader.readInt(inputStream);
        AerodynamicsModel aerodynamicsModel;
        try {
            aerodynamicsModel = AerodynamicsModel.values()[aerodynamicsModelInt];
        } catch (IndexOutOfBoundsException e) {
            throw new PMXLoadException("不可识别的AerodynamicsModel:" + aerodynamicsModelInt);
        }
        final float configVCF = PMXTypeReader.readFloat(inputStream),
                configDP = PMXTypeReader.readFloat(inputStream),
                configDG = PMXTypeReader.readFloat(inputStream),
                configLF = PMXTypeReader.readFloat(inputStream),
                configPR = PMXTypeReader.readFloat(inputStream),
                configVC = PMXTypeReader.readFloat(inputStream),
                configDF = PMXTypeReader.readFloat(inputStream),
                configMT = PMXTypeReader.readFloat(inputStream),
                configCHR = PMXTypeReader.readFloat(inputStream),
                configKHR = PMXTypeReader.readFloat(inputStream),
                configSHR = PMXTypeReader.readFloat(inputStream),
                configAHR = PMXTypeReader.readFloat(inputStream),
                clusterSRHR_CL = PMXTypeReader.readFloat(inputStream),
                clusterSKHR_CL = PMXTypeReader.readFloat(inputStream),
                clusterSSHR_CL = PMXTypeReader.readFloat(inputStream),
                clusterSR_SPLT_CL = PMXTypeReader.readFloat(inputStream),
                clusterSK_SPLT_CL = PMXTypeReader.readFloat(inputStream),
                clusterSS_SPLT_CL = PMXTypeReader.readFloat(inputStream);
        final int interationV_IT = PMXTypeReader.readInt(inputStream),
                interationP_IT = PMXTypeReader.readInt(inputStream),
                interationD_IT = PMXTypeReader.readInt(inputStream),
                interationC_IT = PMXTypeReader.readInt(inputStream),
                materialLST = PMXTypeReader.readInt(inputStream),
                materialAST = PMXTypeReader.readInt(inputStream),
                materialVST = PMXTypeReader.readInt(inputStream);
        final int anchorRigidBodyCount = PMXTypeReader.readInt(inputStream);
        final AnchorRigidBody[] anchorRigidBodies = new AnchorRigidBody[anchorRigidBodyCount];
        for (int i = 0; i < anchorRigidBodyCount; i++) {
            anchorRigidBodies[i] = AnchorRigidBody.load(header, inputStream);
        }
        final int vertexPinCount = PMXTypeReader.readInt(inputStream);
        final int vertexPins[] = new int[vertexPinCount];
        for (int i = 0; i < vertexPinCount; i++) {
            vertexPins[i] = PMXIndexReader.readVertexIndex(global.vertexIndexSize, inputStream);
        }
        return new PMXSoftBodyData(softBodyNameLocal, softBodyNameUniversal, shapeType, materialIndex, group, noCollisionMask, flags, bLinkCreateDistance, numberOfClusters,
                totalMass, collisionMargin, aerodynamicsModel, configVCF, configDP, configDG, configLF, configPR, configVC, configDF, configMT, configCHR, configKHR, configSHR, configAHR, clusterSRHR_CL,
                clusterSKHR_CL, clusterSSHR_CL, clusterSR_SPLT_CL, clusterSK_SPLT_CL, clusterSS_SPLT_CL, interationV_IT, interationP_IT, interationD_IT, interationC_IT, materialLST,
                materialAST, materialVST, anchorRigidBodies, vertexPins);
    }

    public enum ShapeType {
        TRIMESH,//2.1
        ROPE,//2.1
        ;
    }


    @ToString
    @EqualsAndHashCode
    public static class Flags {
        @Getter
        public final boolean bLink,//??? 2.1
                clusterCreation,//??? 2.1
                linkCrossing;//??? 2.1

        public Flags(byte flag) {
            bLink = BitsUtil.bitCheck(flag, 0);
            clusterCreation = BitsUtil.bitCheck(flag, 1);
            linkCrossing = BitsUtil.bitCheck(flag, 2);
        }
    }

    public enum AerodynamicsModel {
        V_POINT,//???
        V_TWOSIDED,//???
        V_ONESIDED,//???
        F_TWOSIDED,//???
        F_ONESIDED//???
    }

    @Data
    public static class AnchorRigidBody {
        private final int rigidBodyIndex;
        private final int vertexIndex;
        private final byte nearMode;

        public static AnchorRigidBody load(PMXHeader header, InputStream inputStream) throws IOException, PMXLoadException {
            val global = header.getGlobal();
            final int rigidBodyIndex = PMXIndexReader.readIndex(global.rigidbodyIndexSize, inputStream);
            final int vertexIndex = PMXIndexReader.readVertexIndex(global.vertexIndexSize, inputStream);
            final byte nearMode = (byte) inputStream.read();
            return new AnchorRigidBody(rigidBodyIndex, vertexIndex, nearMode);
        }
    }
}
