package com.lonacloud.modelloader.pmx.data;

import com.lonacloud.modelloader.pmx.exception.PMXLoadException;
import com.lonacloud.modelloader.pmx.util.PMXIndexReader;
import lombok.Data;
import lombok.val;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author lona, 1307693959@qq.com
 * @version 1.0
 **/
@Data
public class PMXSurfaceData {
    public final int vertexA, vertexB, vertexC;

    public static PMXSurfaceData load(PMXHeader header, InputStream inputStream) throws PMXLoadException, IOException {
        val indexA = PMXIndexReader.readVertexIndex(header.getGlobal().vertexIndexSize, inputStream);
        val indexB = PMXIndexReader.readVertexIndex(header.getGlobal().vertexIndexSize, inputStream);
        val indexC = PMXIndexReader.readVertexIndex(header.getGlobal().vertexIndexSize, inputStream);
        return new PMXSurfaceData(indexA, indexB, indexC);
    }

}
