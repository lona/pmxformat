package com.lonacloud.modelloader.pmx.data;

import com.lonacloud.modelloader.pmx.util.PMXTypeReader;
import lombok.Data;
import lombok.val;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author lona, 1307693959@qq.com
 * @version 1.0
 **/
@Data
public class PMXTextureData {
    public final String path;

    public static PMXTextureData load(PMXHeader header, InputStream inputStream) throws IOException {
        val path = PMXTypeReader.readString(header.getGlobal().getEncoding(), inputStream);
        return new PMXTextureData(path);
    }

}
