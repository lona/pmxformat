package com.lonacloud.modelloader.pmx.data;

import com.lonacloud.modelloader.pmx.util.PMXTypeReader;
import com.lonacloud.modelloader.pmx.deform.PMXBDEFReader;
import com.lonacloud.modelloader.pmx.deform.PMXBoneDeform;
import com.lonacloud.modelloader.pmx.exception.PMXLoadException;
import lombok.Data;
import lombok.val;
import lombok.var;
import org.joml.Vector2f;
import org.joml.Vector3f;
import org.joml.Vector4f;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author lona, 1307693959@qq.com
 * @version 1.0
 **/
@Data
public class PMXVertexData {
    public final Vector3f position, normal;
    public final Vector2f uv;
    public final Vector4f[] additionalVec4;//N is defined in the Globals (Can be 0)
    public final PMXBoneDeform boneDeform;//0 = BDEF1, 1 = BDEF2, 2 = BDEF4, 3 = SDEF, 4 = QDEF
    public final float edgeScale;//Pencil-outline scale (1.0 should be around 1 pixel)

    public static PMXVertexData load(PMXHeader header, InputStream inputStream) throws IOException, PMXLoadException {
        val global = header.getGlobal();
        val additionalVec4Count = global.additionalVec4Count;
        val position = PMXTypeReader.readVec3(inputStream);
        val normal = PMXTypeReader.readVec3(inputStream);
        val uv = PMXTypeReader.readVec2(inputStream);
        final Vector4f[] additionalVec4 = new Vector4f[additionalVec4Count];
        for (var i = 0; i < additionalVec4Count; i++) {
            additionalVec4[i] = PMXTypeReader.readVec4(inputStream);
        }
        PMXBoneDeform boneDeform = PMXBDEFReader.read(global.boneIndexSize, inputStream);
        float edgeScale = PMXTypeReader.readFloat(inputStream);
        return new PMXVertexData(position, normal, uv, additionalVec4, boneDeform, edgeScale);
    }
}
