package com.lonacloud.modelloader.pmx.deform;

import lombok.Data;

/**
 * @author lona, 1307693959@qq.com
 * @version 1.0
 **/
@Data
public class BDEF implements PMXBoneDeform {
    public final PMXDeformType type;
    public final int[] index;
    public final float[] boneWeights;

    @Override
    public PMXDeformType getType() {
        return type;
    }
}
