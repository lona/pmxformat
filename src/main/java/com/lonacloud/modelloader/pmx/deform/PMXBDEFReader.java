package com.lonacloud.modelloader.pmx.deform;

import com.lonacloud.modelloader.pmx.util.PMXTypeReader;
import com.lonacloud.modelloader.pmx.exception.PMXLoadException;
import lombok.val;
import org.joml.Vector3f;

import java.io.IOException;
import java.io.InputStream;

/**
 * PMX Bone Deform Reader
 *
 * @author lona, 1307693959@qq.com
 * @version 1.0
 **/
public abstract class PMXBDEFReader {

    public static PMXBoneDeform read(int indexSize, InputStream inputStream) throws IOException, PMXLoadException {
        final byte weightDeformType = (byte) inputStream.read();
        try {
            val type = PMXDeformType.values()[weightDeformType];
            switch (type) {
                case BDEF1: {
                    final int index = readIndex(indexSize, inputStream);
                    return new BDEF(type, new int[]{index}, new float[0]);
                }
                case BDEF2: {
                    final int[] index = new int[2];
                    final float[] weight = new float[1];
                    index[0] = readIndex(indexSize, inputStream);
                    index[1] = readIndex(indexSize, inputStream);
                    weight[0] = PMXTypeReader.readFloat(inputStream);
                    return new BDEF(type, index, weight);
                }
                case BDEF4: {
                    final int[] index = new int[4];
                    final float[] weight = new float[4];
                    index[0] = readIndex(indexSize, inputStream);
                    index[1] = readIndex(indexSize, inputStream);
                    index[2] = readIndex(indexSize, inputStream);
                    index[3] = readIndex(indexSize, inputStream);
                    weight[0] = PMXTypeReader.readFloat(inputStream);
                    weight[1] = PMXTypeReader.readFloat(inputStream);
                    weight[2] = PMXTypeReader.readFloat(inputStream);
                    weight[3] = PMXTypeReader.readFloat(inputStream);
                    return new BDEF(type, index, weight);
                }
                case SDEF: {
                    final int index1 = readIndex(indexSize, inputStream);
                    final int index2 = readIndex(indexSize, inputStream);
                    final float bone1Weight = PMXTypeReader.readFloat(inputStream);
                    final Vector3f c = PMXTypeReader.readVec3(inputStream);
                    final Vector3f r0 = PMXTypeReader.readVec3(inputStream);
                    final Vector3f r1 = PMXTypeReader.readVec3(inputStream);
                    return new SDEF(index1, index2, bone1Weight, c, r0, r1);
                }
                case QDEF: {
                    final int[] index = new int[4];
                    final float[] weight = new float[4];
                    index[0] = readIndex(indexSize, inputStream);
                    index[1] = readIndex(indexSize, inputStream);
                    index[2] = readIndex(indexSize, inputStream);
                    index[3] = readIndex(indexSize, inputStream);
                    weight[0] = PMXTypeReader.readFloat(inputStream);
                    weight[1] = PMXTypeReader.readFloat(inputStream);
                    weight[2] = PMXTypeReader.readFloat(inputStream);
                    weight[3] = PMXTypeReader.readFloat(inputStream);
                    return new QDEF(index, weight);
                }
                default:
                    throw new PMXLoadException("Unexpected value: " + type);
            }
        } catch (IndexOutOfBoundsException e) {
            throw new PMXLoadException("不可识别的类型:" + weightDeformType);
        }
    }

    private static int readIndex(int indexSize, InputStream inputStream) throws IOException, PMXLoadException {
        switch (indexSize) {
            case 1:
                return inputStream.read();
            case 2:
                return PMXTypeReader.readShort(inputStream);
            case 4:
                return readIndex(indexSize, inputStream);
            default:
                throw new PMXLoadException("错误的IndexSize:" + indexSize);
        }
    }
}
