package com.lonacloud.modelloader.pmx.deform;

/**
 * @author lona, 1307693959@qq.com
 * @version 1.0
 **/
public interface PMXBoneDeform {
    PMXDeformType getType();
}
