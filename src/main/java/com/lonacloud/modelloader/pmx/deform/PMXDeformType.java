package com.lonacloud.modelloader.pmx.deform;

import lombok.AllArgsConstructor;

/**
 * @author lona, 1307693959@qq.com
 * @version 1.0
 **/
@AllArgsConstructor
public enum PMXDeformType {
    BDEF1(1, 0),
    BDEF2(2, 1),
    BDEF4(4, 4),
    SDEF(2, 1),
    QDEF(4, 4),
    ;

    public final int numIndex, numBoneWeight;

}
