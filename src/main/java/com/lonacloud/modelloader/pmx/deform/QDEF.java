package com.lonacloud.modelloader.pmx.deform;

import lombok.Data;

/**
 * Dual quaternion deform blending
 *
 * @author lona, 1307693959@qq.com
 * @version 1.0
 **/
@Data
public class QDEF implements PMXBoneDeform {
    public final int[] index;
    public final float[] boneWeights;

    @Override

    public PMXDeformType getType() {
        return PMXDeformType.QDEF;
    }
}
