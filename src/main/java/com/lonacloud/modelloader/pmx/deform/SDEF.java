package com.lonacloud.modelloader.pmx.deform;

import lombok.Data;
import org.joml.Vector3f;

/**
 * Spherical deform blending
 * @author lona, 1307693959@qq.com
 * @version 1.0
 **/
@Data
public class SDEF implements PMXBoneDeform {
    public final int index1, index2;
    public final float bone1Wight;
    public final Vector3f c, r0, r1;

    @Override
    public PMXDeformType getType() {
        return PMXDeformType.SDEF;
    }

    public float getBone2Wight() {
        return 1.0f - bone1Wight;
    }
}
