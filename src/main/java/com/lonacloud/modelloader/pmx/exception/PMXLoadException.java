package com.lonacloud.modelloader.pmx.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * @author lona, 1307693959@qq.com
 * @version 1.0
 **/
@AllArgsConstructor
@ToString
public class PMXLoadException extends Exception {
    @Getter
    private final String reason;
}
