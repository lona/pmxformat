package com.lonacloud.modelloader.pmx.util;

/**
 * @author lona, 1307693959@qq.com
 * @version 1.0
 **/
public class BitsUtil {

    public static boolean bitCheck(byte flag, int bit) {
        return ((flag >> bit) & 0x01) == 1;
    }
}
