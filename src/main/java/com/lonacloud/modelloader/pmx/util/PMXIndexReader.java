package com.lonacloud.modelloader.pmx.util;

import com.lonacloud.modelloader.pmx.exception.PMXLoadException;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/**
 * @author lona, 1307693959@qq.com
 * @version 1.0
 **/
public class PMXIndexReader {
    public static int readVertexIndex(int indexSize, InputStream inputStream) throws PMXLoadException, IOException {
        switch (indexSize) {
            case 1: {
                byte[] b = new byte[1];
                inputStream.read(b);
                ByteBuffer byteBuffer = ByteBuffer.allocate(2);
                byteBuffer.put((byte) 0);
                byteBuffer.put(b);
                byteBuffer.flip();
                return byteBuffer.getShort();
            }
            case 2: {
                byte[] b = new byte[2];
                inputStream.read(b);
                ByteBuffer byteBuffer = ByteBuffer.allocate(4);
                byteBuffer.put((byte) 0);
                byteBuffer.put((byte) 0);
                byteBuffer.put(b[1]);
                byteBuffer.put(b[0]);
                byteBuffer.flip();
                return byteBuffer.getInt();
            }
            case 4:
                return PMXTypeReader.readInt(inputStream);
            default:
                throw new PMXLoadException("错误的IndexSize:" + indexSize);
        }
    }

    public static int readIndex(int indexSize, InputStream inputStream) throws PMXLoadException, IOException {
        switch (indexSize) {
            case 1:
                return inputStream.read();
            case 2:
                return PMXTypeReader.readShort(inputStream);
            case 4:
                return PMXTypeReader.readInt(inputStream);
            default:
                throw new PMXLoadException("错误的IndexSize:" + indexSize);
        }
    }
}
