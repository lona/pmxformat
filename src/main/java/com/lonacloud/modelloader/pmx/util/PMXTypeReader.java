package com.lonacloud.modelloader.pmx.util;

import lombok.val;
import org.joml.Vector2f;
import org.joml.Vector3f;
import org.joml.Vector4f;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;

/**
 * @author lona, 1307693959@qq.com
 * @version 1.0
 **/
public class PMXTypeReader {
    private static final ByteBuffer buffer = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN);

    public static String readString(Charset charset, InputStream inputStream) throws IOException {
        final int length = readInt(inputStream);
        final byte[] bytes = new byte[length];
        inputStream.read(bytes, 0, length);
        return new String(bytes, charset);
    }

    public static short readShort(InputStream inputStream) throws IOException {
        buffer.rewind();
        inputStream.read(buffer.array(), 0, 2);
        return (short) Short.toUnsignedInt(buffer.getShort());
    }


    public static int readInt(InputStream inputStream) throws IOException {
        buffer.rewind();
        inputStream.read(buffer.array(), 0, 4);
        return buffer.getInt();
    }

    public static float readFloat(InputStream inputStream) throws IOException {
        buffer.rewind();
        inputStream.read(buffer.array(), 0, 4);
        return buffer.getFloat();
    }

    public static double readDouble(InputStream inputStream) throws IOException {
        buffer.rewind();
        inputStream.read(buffer.array(), 0, 8);
        return buffer.getDouble();
    }

    public static Vector2f readVec2(InputStream inputStream) throws IOException {
        val x = readFloat(inputStream);
        val y = readFloat(inputStream);
        return new Vector2f(x, y);
    }

    public static Vector3f readVec3(InputStream inputStream) throws IOException {
        val x = readFloat(inputStream);
        val y = readFloat(inputStream);
        val z = readFloat(inputStream);
        return new Vector3f(x, y, z);
    }

    public static Vector4f readVec4(InputStream inputStream) throws IOException {
        val x = readFloat(inputStream);
        val y = readFloat(inputStream);
        val z = readFloat(inputStream);
        val w = readFloat(inputStream);
        return new Vector4f(x, y, z, w);
    }

    public static byte readFlag(InputStream inputStream) throws IOException {
        return (byte) inputStream.read();
    }

    public static boolean readBool(InputStream inputStream) throws IOException {
        return inputStream.read() != 0;
    }
}
