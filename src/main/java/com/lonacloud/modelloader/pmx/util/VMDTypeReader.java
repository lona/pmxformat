package com.lonacloud.modelloader.pmx.util;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * @author lona, 1307693959@qq.com
 * @version 1.0
 **/
public class VMDTypeReader {
    private static final ByteBuffer buffer = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN);

    public static String readStringVMD(InputStream in, int length) throws IOException {
        byte[] bytes = new byte[length];
        in.read(bytes);
        int end = 0;
        for (byte b : bytes) {
            if (b == 0x00 || b == 0xFD) {
                break;
            }
            end++;
        }
        byte[] strByte = new byte[end];
        System.arraycopy(bytes, 0, strByte, 0, strByte.length);
        return new String(strByte, "Shift-JIS");
    }

    public static short readShort(InputStream inputStream) throws IOException {
        buffer.rewind();
        inputStream.read(buffer.array(), 0, 2);
        return (short) Short.toUnsignedInt(buffer.getShort());
    }

    public static int readInt(InputStream inputStream) throws IOException {
        buffer.rewind();
        inputStream.read(buffer.array(), 0, 4);
        return buffer.getInt();
    }

    public static float readFloat(InputStream inputStream) throws IOException {
        buffer.rewind();
        inputStream.read(buffer.array(), 0, 4);
        return buffer.getFloat();
    }
}
